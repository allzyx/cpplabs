#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  double x;
  double p;
  double mult = 1.0;

  cout << "Enter n: ";
  cin >> x;

  for (int n = 1; n <= 4; n++) {
    p = 1.0 + sqrtf(2 * x / n);
    mult *= p;
  }

  cout << "Multiplication of the series: " << mult << endl;

  return 0;
}
