#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {

  double a, b, c, d;
  double p;

  cout << "a = ";
  cin >> a;
  cout << "b = ";
  cin >> b;
  cout << "c = ";
  cin >> c;
  cout << "d = ";
  cin >> d;

  double part = powf(a, b) + powf(c, 2);
  double first = logf(part);
  double second = powf(sinf(sqrtf(part) / (a * b + c)), 2);

  p = first * second;
  cout << "p = " << p << endl;

  return 0;
}
