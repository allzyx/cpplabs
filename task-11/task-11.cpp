#include <iostream>
#include <math.h>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  double x;
  double x1;
  double value = sqrt(2) / 2;

  cout << "Enter x: ";
  cin >> x;

  x1 = cos(pow(x, 2));
  cout << "cos(x^2) = " << x1 << endl;

  if (x1 > value) {
    cout << x1 << " > sqrt(2)/2" << endl;
    cout << "divide x in half" << endl;
    x /= 2;
  } else {
    cout << x1 << " !> sqrt(2)/2" << endl;
    cout << "doubling x" << endl;
    x *= 2;
  }

  cout << x << endl;
  return 0;
}
