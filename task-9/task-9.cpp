#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  double a, b, c;
  double g;

  cout << "a = ";
  cin >> a;
  cout << "b = ";
  cin >> b;
  cout << "c = ";
  cin >> c;

  double part1 = a * pow(c, 2);
  double part2 = a + 2 * b + 3 * c;

  double first = part1 / part2;
  double second = sqrt(fabs(part1 / part2));
  double third = sin(M_PI * pow(a, 3) + c);

  g = first * second * third;
  cout << "G = " << g << endl;

  return 0;
}
