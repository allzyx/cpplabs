#include <cmath>
#include <iostream>
#include <math.h>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  double s;
  double b, c, d, m, k;

  cout << "Enter the value of b: ";
  cin >> b;
  cout << "Enter the value of c: ";
  cin >> c;
  cout << "Enter the value of d: ";
  cin >> d;
  cout << "Enter the value of m: ";
  cin >> m;
  cout << "Enter the value of k: ";
  cin >> k;

  double part1 = 2 * 1 / tan((d + M_PI / 4) / 3);
  double part2 = 1 / tan((b + M_PI / 4) / m);
  double part3 = pow(1 / tan((c + M_PI / 4) / k), 2);

  s = (part1 - part2) / part3;
  cout << endl << "The value of S is: " << s << endl;

  return 0;
}
