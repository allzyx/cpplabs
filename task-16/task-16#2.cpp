#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int n;
  cout << "Enter size of array: ";
  cin >> n;
  int a[n];

  cout << "Enter array elements: " << endl;
  for (int i = 0; i < n; i++) {
    cout << "a[" << i << "]: ";
    cin >> a[i];
  }

  int min = a[0];
  int max = a[0];

  cout << endl << "Original array: " << endl;
  for (int i = 0; i < n; i++) {
    cout << "a[" << i << "]: " << a[i] << endl;
  }

  for (int i = 0; i < n; i++) {
    if (a[i] < min) {
      min = a[i];
    }
    if (a[i] > max) {
      max = a[i];
    }
  }

  a[0] = max;
  a[n - 1] = min;

  cout << endl << "Modified array: " << endl;
  for (int i = 0; i < n; i++) {
    cout << "a[" << i << "]: " << a[i] << endl;
  }

  return 0;
}
