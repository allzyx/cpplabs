#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int n;
  cout << "Enter size of array: ";
  cin >> n;
  int a[n];

  cout << "Enter array elements: " << endl;
  for (int i = 0; i < n; i++) {
    cout << "a[" << i << "]: ";
    cin >> a[i];
  }

  cout << endl << "Original array: " << endl;
  for (int i = 0; i < n; i++) {
    cout << "a[" << i << "]: " << a[i] << endl;
  }

  for (int i = 0; i < n; i++) {
    if (a[i] == 0) {
      a[i] = 1;
    } else if (a[i] == 1) {
      a[i] = 0;
    }
  }

  cout << endl << "Modified array: " << endl;
  for (int i = 0; i < n; i++) {
    cout << "a[" << i << "]: " << a[i] << endl;
  }

  return 0;
}
