#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int n, m;
  cout << "Enter n: ";
  cin >> n;
  cout << "Enter m: ";
  cin >> m;
  int a[n][m];

  int min;
  int max;

  int sumOfEven = 0;

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      cout << "Enter a[" << i << "][" << j << "]: ";
      cin >> a[i][j];

      if (i == 0 && j == 0) {
        min = a[i][j];
        max = a[i][j];
      }

      if (a[i][j] < min) {
        min = a[i][j];
      }
      if (a[i][j] > max) {
        max = a[i][j];
      }

      if (a[i][j] % 2 == 0) {
        sumOfEven += a[i][j];
      }
    }
  }

  if (min % 2 == 0) {
    sumOfEven -= min;
  }
  if (max % 2 == 0) {
    sumOfEven -= max;
  }

  cout << "Unmodified matrix: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      cout << a[i][j] << " ";
    }
    cout << endl;
  }

  a[0][0] = sumOfEven;
  a[n - 1][m - 1] = sumOfEven;
  a[n - 1][0] = sumOfEven;
  a[0][m - 1] = sumOfEven;

  cout << "Modified matrix: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      cout << a[i][j] << " ";
    }
    cout << endl;
  }

  return 0;
}
