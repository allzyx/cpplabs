#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int n, m;
  cout << "Enter n: ";
  cin >> n;
  cout << "Enter m: ";
  cin >> m;
  int a[n][m];

  int numberOfEven = 0;
  int sumOfEven = 0;
  int avgOfEven = 0;

  int *b = new int[numberOfEven];

  cout << "Enter matrix: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      cout << "a[" << i << "][" << j << "]: " << endl;
      cin >> a[i][j];

      if (a[i][j] % 2 == 0) {
        numberOfEven++;
        sumOfEven += a[i][j];
        b[numberOfEven - 1] = a[i][j];
      }
    }
  }
  avgOfEven = sumOfEven / numberOfEven;

  cout << endl << endl << endl << "Matrix: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      cout << a[i][j] << " ";
    }
    cout << endl;
  }

  cout << endl << "Array of even elements: " << endl;
  for (int i = 0; i < numberOfEven; i++) {
    cout << b[i] << " ";
  }

  cout << endl << endl << "Number of even elements: " << numberOfEven << endl;
  cout << endl << "Sum of even elements: " << sumOfEven << endl;
  cout << endl << "Average of even elements: " << avgOfEven << endl;

  b[numberOfEven - 1] = avgOfEven;

  cout << endl << "Modified array: " << endl;
  for (int i = 0; i < numberOfEven; i++) {
    cout << b[i] << " ";
  }
  cout << endl;

  delete[] b;
  return 0;
}
