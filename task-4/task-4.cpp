#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int n;
  int m;
  int y;

  cout << "Enter n (start): ";
  cin >> n;
  cout << "Enter m (end): ";
  cin >> m;

  for (int x = n; x <= m; x += 2) {
    y = 2 * x - 1;
    cout << "x = " << x << endl;
    cout << "y = " << y << endl << endl;
  }

  return 0;
}
