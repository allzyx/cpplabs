#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int a, b, c;

  cout << "You'll be asked for three integers." << endl;
  cout << "enter a: ";
  cin >> a;
  cout << "enter b: ";
  cin >> b;
  cout << "enter c: ";
  cin >> c;

  if ((a > b && a < c) || (a > c && a < b)) {
    cout << "Middle number is a: " << a;
  } else if ((b > a && b < c) || (b > c && b < a)) {
    cout << "Middle number is b: " << b;
  } else if ((c > a && c < b) || (c > b && c < a)) {
    cout << "Middle number is c: " << c;
  } else {
    cout << "Some of the numbers are equal." << endl;
    cout << "Please enter three different numbers." << endl;
  }

  return 0;
}
