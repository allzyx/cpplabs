#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int a;
  int b;

  cout << "Enter number a: ";
  cin >> a;

  cout << "Enter number b: ";
  cin >> b;

  cout << "a = " << a << endl;
  cout << "b = " << b << endl;

  if (a > b) {
    b = pow(b, 2);
  } else if (a < b) {
    a = pow(a, 2);
  } else {
    cout << "Numbers are equal" << endl;
  }

  cout << endl;
  cout << "a = " << a << endl;
  cout << "b = " << b << endl;

  return 0;
}
