#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  double n;
  double m;
  double y;

  cout << "Enter n (start): ";
  cin >> n;

  cout << "Enter m (end): ";
  cin >> m;

  for (double x = n; x <= m; x += 0.5) {
    y = 1.6 * pow(x, 3) - 1.5;

    if (y >= -2 && y <= -1) {
      cout << endl << "x = " << x << ", y = " << y << endl;
    }
  }

  return 0;
}
