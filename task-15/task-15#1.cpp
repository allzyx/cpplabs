#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int a[11];
  int sumFromEight = 0;

  for (int i = 0; i < 11; i++) {
    cout << "enter a[" << i << "]: ";
    cin >> a[i];
  }

  for (int i = 8; i < 11; i++) {
    sumFromEight += a[i];
  }

  cout << "sum of A[8] to A[10] elemets: " << sumFromEight << endl;

  return 0;
}
