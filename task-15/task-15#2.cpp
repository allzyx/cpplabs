#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int size;
  cout << "enter size of array: ";
  cin >> size;
  int x[size];
  int k = 0;

  for (int i = 0; i < size; i++) {
    cout << "enter x[" << i << "]: ";
    cin >> x[i];
    if (x[i] % 3 == 0) {
      k++;
    }
  }

  for (int i = 0; i < size; i++) {
    cout << "x[" << i << "] = " << x[i] << endl;
  }

  cout << "k = " << k << endl;

  for (int i = 0; i < size; i++) {
    if (x[i] < 0) {
      x[i] = k;
    }
  }

  for (int i = 0; i < size; i++) {
    cout << "x[" << i << "] = " << x[i] << endl;
  }

  return 0;
}
