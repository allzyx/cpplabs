#include <cmath>
#include <iostream>
#include <math.h>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  double a, b, c;
  double x;

  cout << "Enter a: ";
  cin >> a;
  cout << "Enter b: ";
  cin >> b;
  cout << "Enter c: ";
  cin >> c;

  double part1 = (sqrtf(powf(a, 2) + powf(b, 2))) / (0.5 + a * b) - powf(a, -1);
  double part2 = c + fabs(a - b);
  double main = sinf(part1 / part2) + cosf(part1 / part2);

  x = expf(-a / b) * a * main;
  cout << "X: " << x << endl;

  return 0;
}
