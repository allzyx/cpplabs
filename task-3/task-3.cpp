#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  double a;
  double y;

  cout << "Enter a number: ";
  cin >> a;

  for (double x = 0; x <= 1; x += 0.05) {
    y = (sqrtf(a) * sinf(x)) / (x + powf(cosf(x), 2));
    cout << "x = " << x << endl;
    cout << "y = " << y << endl << endl;
  }

  return 0;
}
