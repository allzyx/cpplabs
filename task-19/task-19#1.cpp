#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int n, m;
  cout << "Enter rows: " << endl;
  cin >> n;
  cout << "Enter columns: " << endl;
  cin >> m;
  int a[n][m];

  cout << endl << "Fill array: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      cout << "a[" << i << "][" << j << "] = ";
      cin >> a[i][j];
    }
  }

  cout << endl << "Unmodified Array: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      cout << "a[" << i << "][" << j << "] = " << a[i][j] << endl;
    }
    cout << endl;
  }

  cout << endl << "Modified Array: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      if (a[i][j] < 0) {
        a[i][j] /= 3;
      }
      cout << "a[" << i << "][" << j << "] = " << a[i][j] << endl;
    }
    cout << endl;
  }

  return 0;
}
