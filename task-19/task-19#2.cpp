#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int n;
  cout << "Enter rows and columns: " << endl;
  cin >> n;
  int a[n][n];
  int numberOfElements = 0;
  int avgOfMainDiagonal = 0;

  cout << endl << "Fill array: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      cout << "a[" << i << "][" << j << "] = ";
      cin >> a[i][j];
    }
  }

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if (i == j) {
        numberOfElements++;
        avgOfMainDiagonal += a[i][j];
      }
    }
  }
  cout << endl << "Sum of main diagonal: " << avgOfMainDiagonal << endl;
  cout << endl << "number of elements: " << numberOfElements << endl;
  avgOfMainDiagonal /= numberOfElements;
  cout << endl << "Average of main diagonal: " << avgOfMainDiagonal << endl;

  cout << endl << "Unmodified array: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      cout << " " << a[i][j];
    }
    cout << endl;
  }

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if ((j > i) && (a[i][j] < avgOfMainDiagonal)) {
        a[i][j] = 1;
      } else if ((i > j) && (a[i][j] < avgOfMainDiagonal)) {
        a[i][j] = -1;
      }
    }
  }

  cout << endl << "Modified array: " << endl;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      cout << " " << a[i][j];
    }
    cout << endl;
  }

  return 0;
}
