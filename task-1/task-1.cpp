#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

double mLessThanN(double m, double n);
double mGreaterThanN(double m, double n);
double mEqualsN(double m);

int main() {
  double m;
  double n;
  double z;

  cout << "Enter m: ";
  cin >> m;

  cout << "Enter n: ";
  cin >> n;

  if (m < n) {
    z = mLessThanN(m, n);
  } else if (m > n) {
    z = mGreaterThanN(m, n);
  } else {
    z = mEqualsN(m);
  }

  cout << "z = " << z << endl;

  return 0;
}

double mLessThanN(double m, double n) {
  return sqrt(fabs(m * n)) + tan(M_PI * n);
}

double mGreaterThanN(double m, double n) { return m * pow(n, 3) + 0.75; }

double mEqualsN(double m) { return 100 + m; }
