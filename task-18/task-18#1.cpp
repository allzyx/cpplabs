#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int a[2][5];
  int multOfNegative = 1;
  int numbersOfNegative = 0;
  double geometricMean;

  cout << "Fill array:" << endl;
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 5; j++) {
      cout << "a[" << i << "][" << j << "] = ";
      cin >> a[i][j];
      if (a[i][j] < 0) {
        numbersOfNegative++;
        multOfNegative *= abs(a[i][j]);
      }
    }
  }

  geometricMean = powf(multOfNegative, 1.0 / numbersOfNegative);
  cout << "Geometric mean of negative numbers = " << geometricMean << endl;

  return 0;
}
