#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int rows, columns;
  cout << "Enter rows: " << endl;
  cin >> rows;
  cout << "Enter columns: " << endl;
  cin >> columns;
  int a[rows][columns];
  int sumOfNegative = 0;

  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < columns; j++) {
      cout << "a[" << i << "][" << j << "] = ";
      cin >> a[i][j];
    }
  }

  if (a[rows - 1][columns - 1] < 0) {
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        if (a[i][j] < 0) {
          sumOfNegative += abs(a[i][j]);
        }
      }
    }
    cout << endl << "Sum of negative numbers = " << sumOfNegative << endl;
  } else {
    cout << endl << a[rows - 1][columns - 1] << " is not negative" << endl;
  }

  return 0;
}
