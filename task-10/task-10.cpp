#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {

  double a, b;
  double z;
  double f, g, p;

  cout << "Enter a: ";
  cin >> a;
  cout << "Enter b: ";
  cin >> b;

  z = 1 / 3.0 * powf(a, 1 / 3.0) + 1 / 4.0 * powf(b, 1 / 4.0);
  cout << "z = " << z << endl;

  if (z == 0) {
    f = tanf(z);
    cout << "f = tan(z)" << endl;
    cout << "f = " << f << endl << endl;
  } else if (z < 0) {
    g = acosf(z) + expf(-2);
    cout << "g = arcos(z) + e^(-2)" << endl;
    cout << "g = " << g << endl << endl;
  } else {
    p = p = powf(z, 2) + powf(a, 3) + sqrtf(powf(z, 4) + a * b);
    cout << "p = z^2 + a^3 + sqrt(z^4 + a * b)" << endl;
    cout << "p = " << p << endl << endl;
  }

  return 0;
}
