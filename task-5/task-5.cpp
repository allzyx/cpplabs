#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int y;
  int n, m;
  int s = 0;
  int s1 = 0;

  cout << "enter n (start): " << endl;
  cin >> n;
  cout << "enter m (end): " << endl;
  cin >> m;

  for (int x = n; x < m; x++) {
    y = pow(x, 3) + 1;
    s += y;
  }

  if (s < 50) {
    cout << "s < 50" << endl;

    for (int x = n; x < m; x++) {
      y = pow(x, 3) + 1;
      if (y % 2 == 0) {
        s1 += y;
      }
    }
    cout << "s1 = " << s1 << endl;

  } else {
    cout << "s > 50" << endl;
    cout << "s = " << s << endl;
  }

  return 0;
}
