#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  double a;
  double f;

  cout << "enter a: ";
  cin >> a;

  for (int k = -10; k <= 10; k++) {
    if (k % 2 == 0) {
      f = 12.0 / k;
      cout << "F(" << k << ") = " << f << endl;
    } else {
      f = pow(k, 2);
      cout << "F(" << k << ") = " << f << endl;
    }
  }

  return 0;
}
