#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  int size;
  cout << "Enter size: ";
  cin >> size;
  int a[size];

  int sum = 0;

  cout << endl << "Enter array: " << endl;
  for (int i = 0; i < size; i++) {
    cout << "Enter a[" << i << "]: ";
    cin >> a[i];
  }

  cout << endl << "Unmodified array: " << endl;
  for (int i = 0; i < size; i++) {
    cout << a[i] << endl;
  }

  if (a[0] > 0) {
    for (int i = 0; i < size / 2; i++) {
      sum += a[i];
    }
    for (int i = size / 2; i < size; i++) {
      a[i] = sum;
    }
    cout << endl << "Modified array: " << endl;
    for (int i = 0; i < size; i++) {
      cout << a[i] << endl;
    }

  } else {
    cout << "First element is not positive number" << endl;
    cout << "Array stays unmodified." << endl;
  }

  return 0;
}
