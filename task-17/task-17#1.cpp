#include <cmath>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int isArraySorted(int a[], int size);

int main() {
  int size;
  cout << "Enter size: ";
  cin >> size;
  int a[size];

  cout << endl << "Enter array: " << endl;
  for (int i = 0; i < size; i++) {
    cout << "Enter a[" << i << "]: ";
    cin >> a[i];
  }

  cout << endl << "Unmodified array: ";
  cout << endl << "Array: " << endl;
  for (int i = 0; i < size; i++) {
    cout << a[i] << endl;
  }

  bool sorted = isArraySorted(a, size);

  if (sorted) {
    cout << "Array is sorted" << endl;
    a[0] = pow(a[0], 2);
    a[size - 1] = pow(a[size - 1], 2);
  } else {
    cout << "Array is not sorted" << endl;
    int temp = a[0];
    a[0] = a[size - 1];
    a[size - 1] = temp;
  }

  cout << endl << "Modified array: ";
  cout << endl << "Array: " << endl;
  for (int i = 0; i < size; i++) {
    cout << a[i] << endl;
  }

  return 0;
}

int isArraySorted(int a[], int size) {
  for (int i = 0; i < size - 1; i++) {
    if (a[i] > a[i + 1]) {
      return 0;
    }
  }

  return 1;
}
